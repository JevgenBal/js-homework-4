"use strict"

let num1 = prompt("Enter first number: ");
let num2 = prompt("Enter second number: ");

let operator = prompt("Enter mathematical operation (+, -, *, /)");

function calculate(num1, num2, operator) {
  let result;


  if (operator === "+") {
    result = +num1 + +num2;
  } else if (operator === "-") {
    result = num1 - num2;
  } else if (operator === "*") {
    result = num1 * num2;
  } else if (operator === "/") {
    result = num1 / num2;
  } else {
    result = "You entered incorect value, please enter numbers";
  }

  return result;
}

console.log(calculate(num1, num2, operator));
